import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:frontend/view/add_product_form.dart';
import 'package:frontend/view/update_product_form.dart';

import '../service/product.dart';
import '../service/fetch_product.dart';
import 'package:http/http.dart' as http;

class ProductList extends StatefulWidget {
  const ProductList ({Key? key}) : super(key: key);

  @override
  State<ProductList> createState() => _ProductListState();
}

class _ProductListState extends State<ProductList> {

  List<Product> futureProduct=[];

  void updateState() async {
    
      final datos = await fetchProduct();
      setState(() {
        futureProduct = datos;
      });
    
  }

  void deleteProduct(int id) async {
    await http.delete(Uri.parse('http://192.168.1.99:8000/api/products/$id/'));
    updateState();
  }

  void createProduct(String name, String price) async {
    await http.post(
      Uri.parse('http://192.168.1.99:8000/api/products/'),
      body: jsonEncode({
          'name': name,
          'price': int.parse(price),
          'image': 'https://picsum.photos/200/300',
      }),
      headers: {
        "content-type": "application/json"
      }
    );
    updateState();
  }

  void updateProduct(int id, String name, String price) async {
    await http.put(
      Uri.parse('http://192.168.1.99:8000/api/products/$id/'),
      body: jsonEncode({
          'name': name,
          'price': int.parse(price),
          'image': 'https://picsum.photos/200/300',
      }),
      headers: {
        "content-type": "application/json"
      }
    );
    updateState();
  }

  @override
  Widget build(BuildContext context) {

    if (futureProduct.isEmpty) {
      updateState();
    }

    return Scaffold(

      appBar: AppBar(
        backgroundColor: const Color.fromARGB(0, 0, 0, 0),
        title: const Text(
          'Home',
          style: TextStyle(color: Colors.black)
        ),
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.only(right: 20.0),
            child: IconButton(
              icon: const Icon(Icons.add, color: Color(0xFF460505)),
              tooltip: 'Add product to list',
              onPressed: () {
                showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return AlertDialog(
                      content: DialogForm(
                        popDialog: Navigator.of(context).pop,
                        createProduct: createProduct,
                      ),
                    );
                  }
                );
              }
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(right: 20.0),
            child: IconButton(
              // TODO: add indeterminated circular progress indicator 
              icon: const Icon(Icons.refresh, color: Color(0xFF460505)),
              tooltip: 'Reload list of products',
              onPressed: () {
                updateState();
              }
            ),
            
          ),
        ],
      ),
      
      body: RefreshIndicator(
        onRefresh: () {
          updateState();
          return fetchProduct();
        },
        child: ListView.builder(
          itemCount: futureProduct.length,
          physics: const BouncingScrollPhysics(),
          itemBuilder: (context, index) {
                return Center(
                  child: Card(
                    color: const Color.fromARGB(255, 255, 255, 255),
                    shape: RoundedRectangleBorder(
                      borderRadius: const BorderRadius.all(Radius.circular(12)),
                      side: BorderSide(
                        color: Colors.black.withOpacity(0.2),
                        width: 1,
                      ),
                    ),

                    child: SizedBox(
                      width: 370,
                      height: 120,
                      child: Row(
                        children: <Widget>[
                          const SizedBox(width: 20),
                          Expanded(
                            child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(futureProduct[index].name,
                                      style: const TextStyle(
                                          fontSize: 18,
                                          fontWeight: FontWeight.bold,
                                      )
                                  )
                                ]),
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              IconButton(
                                icon: const Icon(Icons.clear, color: Color(0xFF460505)),
                                tooltip: 'Delete item',
                                
                                onPressed: () {
                                  showDialog(
                                    context: context,
                                    builder: (BuildContext context) {
                                      return AlertDialog(
                                        title: const Text('Are you sure you want to delete this item?'),
                                        actions: [
                                          TextButton(
                                            onPressed: () {
                                              Navigator.of(context).pop();
                                              updateState();
                                            },
                                            child: const Text('Cancel'),
                                          ),
                                          TextButton(
                                            onPressed: () {
                                              deleteProduct(futureProduct[index].id);
                                              Navigator.of(context).pop();
                                            },
                                            child: const Text('Confirm'),
                                          ),
                                        ],
                                      );
                                    }
                                  );
                                },
                              ),
                              IconButton(
                                icon: const Icon(Icons.open_in_new_sharp, color: Color(0xFF460505)),
                                tooltip: 'Update product',
                                onPressed: () {
                                  showDialog(
                                    context: context,
                                    builder: (BuildContext context) {
                                      return AlertDialog(
                                        content: DialogUpdateForm(
                                          popDialog: Navigator.of(context).pop,
                                          product: futureProduct[index],
                                          updateProduct: updateProduct,
                                        ),
                                      );
                                    }
                                  );
                                }
                              ),
                            ]
                          ),
                        ],
                      )
                    )

                  )
                );
              },
        )
    )
      
    );
  }
}