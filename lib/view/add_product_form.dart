import 'package:flutter/material.dart';

class DialogForm extends StatefulWidget {

  final Function popDialog;
  final void Function(String name, String price) createProduct;

  const DialogForm({super.key, required this.popDialog, required this.createProduct});

  @override
  State<DialogForm> createState() => _DialogFormState();
}

class _DialogFormState extends State<DialogForm> {
  final _nameController = TextEditingController();
  final _priceController = TextEditingController();

  @override
  void dispose() {
    _nameController.dispose();
    _priceController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        const Text(
          'Name',
          textAlign: TextAlign.left,
          style: TextStyle(color: Color.fromARGB(255, 105, 105, 105))
        ),
        SizedBox(
          height: 20,
          child: TextField(
            controller: _nameController,
          ),
        ),
        const SizedBox(
          height: 22,
        ),
        const Text(
          'Price',
          textAlign: TextAlign.left,
          style: TextStyle(color: Color.fromARGB(255, 105, 105, 105))
        ),
        SizedBox(
          height: 20,
          child: TextField(
            controller: _priceController,
          ),
        ),
        SizedBox(
          height: 100,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              TextButton(
                onPressed: () {
                  widget.popDialog();
                },
                child: const Text('Cancel'),
              ),
              TextButton(
                onPressed: () {
                  widget.createProduct(
                    _nameController.text,
                    _priceController.text,
                  );
                  widget.popDialog();
                },
                child: const Text('Confirm'),
              ),
            ],
          )
        ),
      ]
    );
  }
}